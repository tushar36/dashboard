import '../styles/globals.css'
import 'aws-sdk'


function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp

import styles from '../styles/Home.module.css'
import AWSGraph from './aws' 
import SentryGraph from './sentry'
import 'antd/dist/antd.css';
import { Form, Input, Button, Checkbox } from 'antd';
import axios from 'axios'
import React, { useState, useEffect } from 'react';

export default function Home() {

  const [session,setsession] = useState(false) 
  const onFinish = async (values) => {
    const res = await axios.post(`/api/auth`, values)
    setsession(()=>{
      console.log(res.data.status)
      return res.data.status
    })
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };
  if(session)
  {return (
    <div className={styles.container}>
      <AWSGraph title="Request Count" endpoint="reqcount" />
      <AWSGraph title="5XX" endpoint="5xx" />
      <AWSGraph title="Latency" endpoint="latency" />
      <SentryGraph title="Sentry Weekly Issues" endpoint="sentry"/>
    </div>
  )}

  else{
    return (
      <Form
        {...layout}
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
  
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
  
        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>
  
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

import React, { useState, useEffect } from 'react';
import {Bar, Line} from 'react-chartjs-2';
import axios from 'axios';
import { get } from 'https';

const  Graph =  ({title, endpoint}) =>{
    const options = {
        scales: {
             xAxes: [{
                 stacked: true
             }],
             yAxes: [{
                 stacked: true
             }]
         }
     }
     function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
      }
      
    const [data, setData] = useState({
        labels: [],
        datasets: [
          {
            label: `${title}`,
            fill: true,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
          }
        ]
      })

    const [stack,setStack] = useState({
            datasets:[],
            labels:[]
          
    })
    useEffect(()=>{
        InitialData()
      },[])
      const InitialData = async () =>{
        let endDate = new Date();
        let startDate = new Date(endDate.getFullYear(),endDate.getMonth(),endDate.getDate()-6)
        const res = await axios.post(`/api/${endpoint}`, {start:startDate.toISOString(),end:endDate.toISOString()})
        console.log(res)
        setData((prev)=>{
          const newData = {...prev,
            labels: res.data.Timestamps.reverse() , datasets:[{...prev.datasets[0], data:res.data.Values.reverse()}]}; 
          return newData;
        })
        setStack(()=>{
            var datasets = []
            const stackgraphValues = res.data.stackgraphValues 
            for(var x in stackgraphValues)
            {
                datasets.push({
                    label:`${x} Errors`,
                    data:stackgraphValues[x].reverse(),
                    backgroundColor:getRandomColor()
                })
            }

            const newData = {labels:res.data.Timestamps, datasets:datasets}
            return newData;
        })
      }

 
     
    return(<div>
<Line  data={data} width={400}
      height={400}/>
<Bar data={stack} options={options} width={400}
      height={400}/>
      </div>
      )
}

export default Graph
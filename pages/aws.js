import React, { useState, useEffect } from 'react';
import {Line} from 'react-chartjs-2';
import { Button, DatePicker, Space } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios'


const  Graph =  ({title, endpoint}) => {
  const [time,setTime] = useState({start:"", end:""})
  const [data, setData] = useState({
    labels: [],
    datasets: [
      {
        label: `${title}`,
        fill: true,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: []
      }
    ]
  })
  function onChangeStart(date, dateString) {
    if (date && date['_d'])
    {console.log(date['_d'].getTime());
    setTime((prev)=>{
      return{
        start: date['_d'].toISOString(),
        end: prev.end
      }
    })
  }
  }
  function onChangeEnd(date, dateString) {
    if (date && date['_d'])
    {console.log(date['_d'].getTime());
    setTime((prev)=>{
      return{
        start: prev.start,
        end: date['_d'].toISOString()
      }
    })
  }
  }

  const sendRequest = async () =>{
    if(time.start>=time.end)
    {
        alert("End Date should be greater than Start Date")
    }
    else{
    const res = await axios.post(`/api/${endpoint}`, time)
    console.log(res)
    setData((prev)=>{
      const newData = {...prev,
        labels: res.data.Timestamps.reverse() , datasets:[{...prev.datasets[0], data:res.data.Values.reverse()}]}; 
      return newData;
    })
  }
  }
  useEffect(()=>{
    InitialData()
  },[])
  const InitialData = async () =>{
    let endDate = new Date();
    let startDate = new Date(endDate.getFullYear(),endDate.getMonth(),endDate.getDate()-6)
    const res = await axios.post(`/api/${endpoint}`, {start:startDate.toISOString(),end:endDate.toISOString()})
    console.log(res)
    setData((prev)=>{
      const newData = {...prev,
        labels: res.data.Timestamps.reverse() , datasets:[{...prev.datasets[0], data:res.data.Values.reverse()}]}; 
      return newData;
    })
  }

console.log(data)
  return(<div>
    <h2>{title}</h2>
    <Space direction="vertical">
    <DatePicker onChange={onChangeStart} />
    <DatePicker onChange={onChangeEnd} />
    <Button onClick={sendRequest}>Submit</Button>
    </Space>
    <Line
      data={data}
      width={400}
      height={400}
    />
  </div>
  )
  };

export default Graph
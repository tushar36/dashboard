import 'aws-sdk'
import cloudwatch from '../setting'
import buildParams from '../parambuilder'

export default async (req, res) => {
  const { method } = req
  switch (method) { 

    case "POST":
      cloudwatch.getMetricData(buildParams(req.body.start,req.body.end,"RequestCount"), function(err, data) {
        if (err) console.log(err, err.stack);
        else     res.status(200).json(data.MetricDataResults[0]);
      });
  }

}




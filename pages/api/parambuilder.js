const build = (start, end, metric) =>{

    var params = {
        "StartTime": start,
        "EndTime": end,
        "LabelOptions": {
          "Timezone" : "-0400"
          },
        "MetricDataQueries": [
          {
            "Id": "m1",
            "Label": "reqcount",
            "MetricStat": {
              "Metric": {
                "Namespace": "AWS/ApplicationELB",
                "MetricName": metric,
                "Dimensions": [
                  {
                    "Name": "LoadBalancer",
                    "Value": "app/zomentum-prod-elb/bd1f488fcd96569e"
                  },
                  {
                    "Name": 'TargetGroup', 
                    "Value": 'targetgroup/zomen-prod-target/c6db724d0117a3d6'
                  },
                ]
              },
              "Period": 86400,
              "Stat": "Sum",
            }
          },        
        ]
      }
return params
}

export default build
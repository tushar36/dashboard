import axios from 'axios'

var cache = 0;
setInterval(()=>{
  cache = 0
},60000*60)

var fetchdata =  async (url,list=[], check="true") =>
{
  const token = process.env.Sentry
  if(check == "false")
  {return list;}
  var result = await axios.get(url,{headers:{"Authorization": `Bearer ${token}`}})
  var next_data = result.headers.link.split(',')[1].split(";")
  url = next_data[0].trim().slice(1, -1);
  check = next_data[2].trim().split('=')[1].slice(1,-1)
  list.push(...result.data)
  return await fetchdata(url=url,list=list,check=check) 
}

export default async (req, res) => {
  const { method } = req
  switch (method) { 

    case "POST":
      if (cache == 0)
      { console.log('abc')
        console.log(cache)
        let endDate = new Date();
      endDate = new Date(endDate.getFullYear(),endDate.getMonth(),endDate.getDate())
      var prevlength = 0;
      var Values = [];
      var Timestamps= [];
      var stackgraphValues = {};
      const categories = ["Server","Type","Reference","ChunkLoad"]
      for (var x in categories)
      {
        stackgraphValues[categories[x]] = []
      }
      stackgraphValues["Other"] = []
        for(var i=1;i<=7;i++)
        {
          var data = await fetchdata(`https://sentry.io/api/0/projects/channelforce/zomentum-fe/issues/?query=is:unresolved age:-${24*i}h`);
          var length = data.length
          data = data.slice(prevlength, length);
          prevlength = length
          Values.push(data.length)
          var other = 0;
          for (var x in categories)
          {
            var result = data.filter(d => d['title'].startsWith(categories[x]));
            stackgraphValues[categories[x]].push(result.length)
            other = other + result.length
          }
          stackgraphValues["Other"].push(data.length-other)
          Timestamps.push(endDate)
          endDate = new Date(endDate.getFullYear(),endDate.getMonth(),endDate.getDate()-1)
        }
        cache = {Values:Values,Timestamps:Timestamps,stackgraphValues:stackgraphValues};
      }
        console.log(cache)
        res.status(200).json(cache);
  }

}
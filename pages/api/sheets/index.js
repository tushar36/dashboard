import axios from 'axios'

export default async (req, res) => {
  const { method } = req
  switch (method) { 

    case "POST":
        const result = await axios.get(`https://spreadsheets.google.com/feeds/cells/1R3iOOCLkrP72VGdUje4muhXd38jwNWDwQuSudjyr3WM/od6/public/basic?alt=json`)
        const data_list  = result.data.feed.entry
        var Timestamps = []
        var Values = []
        for (var i = 3; i < data_list.length; i=i+3) {
        Timestamps.push(data_list[i].content['$t']);
        }
        for (var i = 4; i < data_list.length; i=i+3) {
        Values.push(data_list[i].content['$t']);
        }
        res.status(200).json({Timestamps:Timestamps,Values:Values});
  }

}
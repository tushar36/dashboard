import AWS from 'aws-sdk' 

var settings = {
	accessKeyId: process.env.KeyId,		
	secretAccessKey:process.env.Secret,
	region: process.env.Region,
}

AWS.config.update(settings);

var cloudwatch = new AWS.CloudWatch(settings)

export default cloudwatch
